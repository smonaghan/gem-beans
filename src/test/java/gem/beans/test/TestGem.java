package gem.beans.test;

import gem.beans.annotation.Attribute;
import gem.beans.annotation.Gem;
import gem.beans.annotation.Gid;
import gem.beans.annotation.Relationship;

@Gem
public class TestGem {

    @Gid
    private String id;

    @Attribute(value = "colour", namespace = "http://example.com/visuals")
    private String color;

    @Attribute
    private boolean active;

    @Relationship(value="foe", namespace="example.org/warfare")
    private TestGem relatedGem1;

    @Relationship
    private TestGem relatedGem2;

    // TODO: consider implication of the noarg constructor
    public TestGem() {
    }

    public TestGem(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public TestGem getRelatedGem1() {
        return relatedGem1;
    }

    public void setRelatedGem1(TestGem relatedGem1) {
        this.relatedGem1 = relatedGem1;
    }

    public TestGem getRelatedGem2() {
        return relatedGem2;
    }

    public void setRelatedGem2(TestGem relatedGem2) {
        this.relatedGem2 = relatedGem2;
    }
}
