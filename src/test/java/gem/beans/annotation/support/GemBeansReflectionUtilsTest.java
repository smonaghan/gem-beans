package gem.beans.annotation.support;

import gem.beans.annotation.Attribute;
import gem.beans.annotation.Gem;
import gem.beans.annotation.Gid;
import gem.beans.annotation.Relationship;
import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Collection;

import static org.junit.Assert.*;

public class GemBeansReflectionUtilsTest {

    @Test
    public void testGetGidField() throws Exception {
        BaseGem gem = new BaseGem("1234");
        Class clazz = gem.getClass();
        Field gidField = GemBeansReflectionUtils.getGidField(clazz);
        assertNotNull(gidField);
        assertEquals(gidField.getName(), "id");
        assertEquals(gidField.getType().getCanonicalName(), String.class.getCanonicalName());
        assertEquals(gidField.get(gem), "1234");
    }

    @Test
    public void testGetGidFieldNotFound() {
        Class clazz = "".getClass();
        Field gidField = GemBeansReflectionUtils.getGidField(clazz);
        assertNull(gidField);
    }

    @Test
    public void testGetGemMeta() {
        Car datsun = new Car("1234");
        Annotation meta = GemBeansReflectionUtils.getGemAnnotation(datsun.getClass());
        assertNotNull(meta);
        assertEquals(meta.annotationType().getCanonicalName(), Gem.class.getCanonicalName());

        Gem gemMeta = (Gem) meta;
        assertEquals("vehicle", gemMeta.namespace());
        assertEquals(ReflectionDirective.TARGET_NAME, gemMeta.value());
    }

    @Test
    public void testGetGemMetaNotFound(){
        Class clazz = "".getClass();
        Annotation meta = GemBeansReflectionUtils.getGemAnnotation(clazz);
        assertNull(meta);
    }
    @Test
    public void testGetAttributes() throws Exception{
        Car subaru = new Car("1234");
        Collection<Field> attributes = GemBeansReflectionUtils.getAttributes(subaru.getClass());
        assertNotNull(attributes);
        assertEquals(3, attributes.size());

        Field vehicleMake = (Field)attributes.toArray()[0];
        Attribute vehicleMakeMeta = vehicleMake.getAnnotation(Attribute.class);
        assertNotNull(vehicleMakeMeta);
        assertEquals("vehicleMake", vehicleMakeMeta.value());

        Field model = (Field)attributes.toArray()[1];
        Attribute modelMeta = model.getAnnotation(Attribute.class);
        assertEquals(ReflectionDirective.TARGET_NAME, modelMeta.value());

        Field color = (Field)attributes.toArray()[2];
        Attribute colorMeta = color.getAnnotation(Attribute.class);
        assertNotNull(colorMeta);
        assertEquals(ReflectionDirective.TARGET_NAME, colorMeta.value());
        assertEquals("visual", colorMeta.namespace());
    }
}

@Gem
class BaseGem {
    @Gid
    String id;

    BaseGem(String id) {
        this.id = id;
    }
}

// demonstrates explicit namespace and implicit entity type
@Gem(namespace = "vehicle")
class Car extends BaseGem {

    // demonstrates explicit attribute name and implicit namespace
    @Attribute(value = "vehicleMake")
    String make;

    // demonstrates implicit attribute name and namespace
    @Attribute
    String model;

    // demonstrates explicit namespace and implicit attribute name
    @Attribute(namespace = "visual")
    String color;

    Car(String id) {
        super(id);
    }
}

@Gem(value = "HumanBeing", namespace = "living")
class Person extends BaseGem {

    Person(String id) {
        super(id);
    }

    @Attribute("firstName")
    String fName;

    @Attribute("middleName")
    String mName;

    @Attribute("familyName")
    String lName;

    @Relationship("mother")
    Person mom;

    @Relationship("father")
    Person dad;
}
