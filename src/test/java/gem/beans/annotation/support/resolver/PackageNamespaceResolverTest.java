package gem.beans.annotation.support.resolver;

import gem.beans.test.TestGem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PackageNamespaceResolverTest {

    @Test
    public void testPackageNamespaceResolver() throws Exception{
        TestGem testGem = new TestGem("1234");
        PackageNamespaceResolver resolver = new PackageNamespaceResolver();

        Class clazz = testGem.getClass();
        assertEquals("gem.beans.test", resolver.resolve(clazz, clazz.getDeclaredField("active")));
    }
}
