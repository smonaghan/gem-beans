package gem.beans.annotation.support.resolver;

import gem.beans.test.TestGem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ReversePackageNamespaceResolverTest {

    @Test
    public void testPackageNamespaceResolver() throws Exception{
        TestGem testGem = new TestGem("1234");
        ReversePackageNamespaceResolver resolver = new ReversePackageNamespaceResolver();

        Class clazz = testGem.getClass();
        assertEquals("test.beans.gem", resolver.resolve(clazz, clazz.getDeclaredField("active")));
    }
}