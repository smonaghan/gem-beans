package gem.beans.annotation.support.resolver;

import gem.beans.test.TestGem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProvidedAttributeLabelResolverTest {

    @Test
    public void testProvidedAttributeLabelResolver() throws Exception{
        TestGem testGem = new TestGem("1234");

        ProvidedAttributeLabelResolver resolver = new ProvidedAttributeLabelResolver();

        Class clazz = testGem.getClass();
        assertEquals("colour", resolver.resolve(clazz, clazz.getDeclaredField("color")));
    }
}