package gem.beans.annotation.support.resolver;

import gem.beans.test.TestGem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProvidedAttributeNamespaceResolverTest {

    @Test
    public void testProvidedAttributeLabelResolver() throws Exception{
        TestGem testGem = new TestGem("1234");

        ProvidedAttributeNamespaceResolver resolver = new ProvidedAttributeNamespaceResolver();

        Class clazz = testGem.getClass();
        assertEquals("http://example.com/visuals", resolver.resolve(clazz, clazz.getDeclaredField("color")));
    }
}