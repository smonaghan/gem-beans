package gem.beans.annotation.support.resolver;

import gem.beans.test.TestGem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProvidedRelationshipLabelResolverTest {

    @Test
    public void testProvidedRelationshipLabelResolver() throws Exception{
        TestGem testGem = new TestGem("1234");

        ProvidedRelationshipLabelResolver resolver = new ProvidedRelationshipLabelResolver();

        Class clazz = testGem.getClass();
        assertEquals("foe", resolver.resolve(clazz, clazz.getDeclaredField("relatedGem1")));
    }
}