package gem.beans.annotation.support.resolver;

import gem.beans.annotation.Gem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProvidedGemNamespaceResolverTest {

    @Test
    public void testProvidedGemNamespaceResolver() throws Exception{
        Sauron sauron = new Sauron();

        ProvidedGemNamespaceResolver resolver = new ProvidedGemNamespaceResolver();

        Class clazz = sauron.getClass();
        assertEquals("example.org/tolkien", resolver.resolve(clazz, null));
    }

}

@Gem(namespace="example.org/tolkien")
class Sauron{}