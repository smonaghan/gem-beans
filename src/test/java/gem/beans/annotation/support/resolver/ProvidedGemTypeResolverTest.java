package gem.beans.annotation.support.resolver;

import gem.beans.annotation.Gem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProvidedGemTypeResolverTest {

    @Test
    public void testProvidedGemTypeResolver() throws Exception{
        Foo fooGem = new Foo();

        ProvidedGemTypeResolver resolver = new ProvidedGemTypeResolver();

        Class clazz = fooGem.getClass();
        assertEquals("FooBerry", resolver.resolve(clazz, null));
    }

}


@Gem("FooBerry")
class Foo{}