package gem.beans.annotation.support.resolver;

import gem.beans.annotation.Gem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ClassNameGemTypeResolverTest {

    @Test
    public void testClassNameGemTypeResolver() throws Exception{
        Bar barGem = new Bar();
        ClassNameGemTypeResolver resolver = new ClassNameGemTypeResolver();
        Class clazz = barGem.getClass();
        assertEquals("Bar", resolver.resolve(clazz, null));
    }

}


@Gem()
class Bar{}