package gem.beans.annotation.support.resolver;

import gem.beans.test.TestGem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProvidedRelationshipNamespaceResolverTest {

    @Test
    public void testProvidedRelationshipNamespaceResolver() throws Exception{
        TestGem testGem = new TestGem("1234");

        ProvidedRelationshipNamespaceResolver resolver = new ProvidedRelationshipNamespaceResolver();

        Class clazz = testGem.getClass();
        assertEquals("example.org/warfare", resolver.resolve(clazz, clazz.getDeclaredField("relatedGem1")));
    }
}