package gem.beans.annotation.support.resolver;

import gem.beans.test.TestGem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FieldNameLabelResolverTest {

    @Test
    public void testFieldNameLabelResolver() throws Exception{
        TestGem testGem = new TestGem("1234");
        FieldNameLabelResolver resolver = new FieldNameLabelResolver();

        Class clazz = testGem.getClass();
        assertEquals("active", resolver.resolve(clazz, clazz.getDeclaredField("active")));
    }
}