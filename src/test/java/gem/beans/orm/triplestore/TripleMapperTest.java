package gem.beans.orm.triplestore;

import gem.beans.orm.triplestore.support.RDF;
import gem.beans.test.TestGem;
import gem.beans.orm.triplestore.Triple;
import static org.junit.Assert.*;

import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;

public class TripleMapperTest {

    @Test
    public void testToGemFromSimpleGraph() {
        String subject = "gem://test.beans.gem/TestGem#1234";
        Triple type = new Triple(subject, RDF.RDF_TYPE, TestGem.class.getName());
        Triple blue = new Triple(subject, "http://example.com/visuals/colour", "blue");

        Collection<Triple> triples = new ArrayList<Triple>();
        triples.add(type);
        triples.add(blue);

        Collection<Object> gems = new TripleMapper().toGem(triples);
        assertEquals(1, gems.size());
        System.out.println("Gem here: " + gems.iterator().next());
    }
}
