package gem.beans.orm.triplestore.applicator;

import gem.beans.test.TestGem;
import gem.beans.orm.triplestore.Triple;
import static org.junit.Assert.*;

import org.junit.Test;

import java.lang.reflect.Field;

public class AttributeFieldApplicatorTest {

    @Test
    public void testApply() throws Exception {
        TestGem gem = new TestGem("1234");
        gem.setColor("blue");
        AttributeFieldApplicator attributeFieldApplicator = new AttributeFieldApplicator();

        Triple colorTriple = new Triple();
        Field colorField = gem.getClass().getDeclaredField("color");
        colorTriple = attributeFieldApplicator.apply(gem, colorField, colorTriple);

        // assert application reverse namespace resolver and default gem namespace
        assertEquals("gem://test.beans.gem/TestGem#1234", colorTriple.getSubject());

        // assert application of specified namespace
        assertEquals("http://example.com/visuals/colour", colorTriple.getPredicate());

        // assert adoption of field name as object
        assertEquals("blue", colorTriple.getObject());
    }
}
