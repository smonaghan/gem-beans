package gem.beans.orm;

import gem.beans.orm.triplestore.Graph;
import gem.beans.orm.triplestore.Triple;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

public class GraphTest {

    @Test
    public void testRdfType() {
        Triple t1 = new Triple("gem://foo#1234", "gem://color", "blue");
        Triple t2 = new Triple("gem://foo#1234", "rdf:type", "com.foo.Bar");

        Graph g = new Graph(t1);
        assertNull(g.getRdfType());


        g.addTriple(t2);
        assertEquals("com.foo.Bar", g.getRdfType());
    }

    @Test
    public void testRoot() {
        Triple t1 = new Triple("gem://foo#1234", "gem://color", "blue");
        Graph g = new Graph(t1);
        assertEquals("gem://foo#1234", g.getRoot());
    }

    @Test
    public void testTriplesSize(){
        Triple t1 = new Triple("gem://foo#1234", "gem://color", "blue");
        Triple t2 = new Triple("gem://foo#1234", "rdf:type", "com.foo.Bar");

        Graph g = new Graph(t1);
        assertEquals(1, g.getTriples().size());

        g.addTriple(t2);
        assertEquals(2, g.getTriples().size());

    }
}
