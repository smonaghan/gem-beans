package gem.beans.orm.util;

import gem.beans.orm.triplestore.Triple;
import gem.beans.orm.triplestore.util.TripleUtils;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

public class TripleUtilsTest {

    private Collection<Triple> triples;

    @Test
    public void testGetDistinctSubjects() {
        Set<String> distinctSubjects = TripleUtils.getDistinctSubjects(triples);
        assertEquals(4, distinctSubjects.size());
        assertTrue(distinctSubjects.contains("gem://foo#bar"));
        assertTrue(distinctSubjects.contains("gem://foo#happy/chicken"));
        assertTrue(distinctSubjects.contains("auto://car#1234"));
        assertTrue(distinctSubjects.contains("gem://pookie#woobie"));
    }

    @Test
    public void testGetDistinctPredicates() {
        Set<String> distinctPredicates = TripleUtils.getDistinctPredicates(triples);
        assertEquals(6, distinctPredicates.size());
        assertTrue(distinctPredicates.contains("gem://color"));
        assertTrue(distinctPredicates.contains("gem://age"));
        assertTrue(distinctPredicates.contains("gem://external"));
        assertTrue(distinctPredicates.contains("auto://make"));
        assertTrue(distinctPredicates.contains("auto://model"));
        assertTrue(distinctPredicates.contains("auto://color"));
    }

    @Test
    public void testGetDistinctObjects() {
        Set<Object> distinctObjects = TripleUtils.getDistinctObjects(triples);
        assertEquals(5, distinctObjects.size());
        assertTrue(distinctObjects.contains("blue"));
        assertTrue(distinctObjects.contains(36));
        assertTrue(distinctObjects.contains("http://example.org"));
        assertTrue(distinctObjects.contains("Honda"));
        assertTrue(distinctObjects.contains("Accord"));
    }

    @Test
    public void testIsRdfType(){
        assertFalse(TripleUtils.isRdfType(new Triple("gem://foo#bar", "gem://color", "blue")));
        assertTrue(TripleUtils.isRdfType(new Triple("gem://foo#bar", "rdf:type", "com.foo.Bar")));
        assertTrue(TripleUtils.isRdfType(new Triple("gem://foo#bar", "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", "com.foo.Bar")));
    }

    @Test
    public void tesGetURI(){
        URI uri = TripleUtils.assembleURI("http://example.com/visuals/colour");
        assertNotNull(uri);

    }

    @Before
    public void setUp() {
        triples = new ArrayList<Triple>();
        triples.add(new Triple("gem://foo#bar", "gem://color", "blue"));
        triples.add(new Triple("gem://foo#bar", "gem://age", 36));
        triples.add(new Triple("gem://pookie#woobie", "gem://age", 36));
        triples.add(new Triple("gem://foo#happy/chicken", "gem://external", "http://example.org"));
        triples.add(new Triple("auto://car#1234", "auto://make", "Honda"));
        triples.add(new Triple("auto://car#1234", "auto://model", "Accord"));
        triples.add(new Triple("auto://car#1234", "auto://color", "blue"));
    }


}
