package gem.beans.annotation;

import gem.beans.annotation.support.ReflectionDirective;
import gem.beans.annotation.support.resolver.*;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to indicate the that host field is an attribute of the associated {@link Gem}
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Attribute {

    /**
     * The semantic name of the associated field
     *
     * @return
     */
    String value() default ReflectionDirective.TARGET_NAME;

    Class<? extends LabelResolver> labelResolver() default ProvidedAttributeLabelResolver.class;

    /**
     * The "namespace" under which which the Attribute has its semantics
     *
     * @return
     */
    String namespace() default ReflectionDirective.PACKAGE;

    Class<? extends NamespaceResolver> namespaceResolver() default ProvidedAttributeNamespaceResolver.class;
}
