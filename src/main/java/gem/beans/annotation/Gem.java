package gem.beans.annotation;

import gem.beans.annotation.support.ReflectionDirective;
import gem.beans.annotation.support.resolver.LabelResolver;
import gem.beans.annotation.support.resolver.NamespaceResolver;
import gem.beans.annotation.support.resolver.ProvidedGemNamespaceResolver;
import gem.beans.annotation.support.resolver.ProvidedGemTypeResolver;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to indicate the that host is a {@link Gem}
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Gem {

    /**
     * The semantic name of the associated type
     *
     * @return
     */
    String value() default ReflectionDirective.TARGET_NAME;

    Class<? extends LabelResolver> typeResolver() default ProvidedGemTypeResolver.class;

    /**
     * The "namespace" under which which the Attribute has its semantics
     *
     * @return
     */
    String namespace() default ReflectionDirective.PACKAGE;

    Class<? extends NamespaceResolver> namespaceResolver() default ProvidedGemNamespaceResolver.class;
}
