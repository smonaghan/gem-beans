package gem.beans.annotation.support;

/**
 * Defines a collection of identifiers indicating specific runtime behavior
 */

public class ReflectionDirective {

    /**
     * Indicates that the reflection system should resolve the name of the Annotation's host
     * {@link java.lang.annotation.ElementType}
     */
    public static final String TARGET_NAME = "TARGET_NAME";

    /**
     * Indicates that the reflection system should resolve the package name of the Annotation's host
     * {@link java.lang.annotation.ElementType}
     */
    public static final String PACKAGE = "PACKAGE";

    /**
     * Indicates that the reflection system should resolve the namespace of Gem to which the Annotation's host is
     * associated
     */
    public static final String ENTITY_NAMESPACE = "ENTITY_NAMESPACE";

}
