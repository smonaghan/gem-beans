package gem.beans.annotation.support.resolver;

import gem.beans.annotation.Attribute;
import gem.beans.annotation.Relationship;
import gem.beans.annotation.support.ReflectionDirective;

import java.lang.reflect.Field;

public class ProvidedRelationshipNamespaceResolver implements NamespaceResolver {
    @Override
    public String resolve(Class clazz, Field field) {
        Relationship relationship = field.getAnnotation(Relationship.class);
        String namespace = relationship.namespace();
        if (namespace.equals(ReflectionDirective.PACKAGE)) {
            NamespaceResolver resolver = new ReversePackageNamespaceResolver();
            return resolver.resolve(clazz, field);
        } else {
            return relationship.namespace();
        }
    }
}
