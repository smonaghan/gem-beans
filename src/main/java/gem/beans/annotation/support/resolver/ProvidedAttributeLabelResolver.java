package gem.beans.annotation.support.resolver;

import gem.beans.annotation.Attribute;
import gem.beans.annotation.support.ReflectionDirective;

import java.lang.reflect.Field;

public class ProvidedAttributeLabelResolver implements LabelResolver {

    @Override
    public String resolve(Class clazz, Field field) {
        Attribute attribute = field.getAnnotation(Attribute.class);
        String label = attribute.value();
        if(label.equals(ReflectionDirective.TARGET_NAME)){
            LabelResolver resolver = new FieldNameLabelResolver();
            return resolver.resolve(clazz, field);
        }else{
            return attribute.value();
        }
    }
}
