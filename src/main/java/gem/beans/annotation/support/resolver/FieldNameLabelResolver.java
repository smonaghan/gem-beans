package gem.beans.annotation.support.resolver;

import java.lang.reflect.Field;

public class FieldNameLabelResolver implements LabelResolver {

    @Override
    public String resolve(Class clazz, Field field) {
        // TODO: need to figure out how to have annotation's value, if assigned, override this behavior
        return field.getName();
    }
}
