package gem.beans.annotation.support.resolver;

import java.lang.reflect.Field;

public interface Resolver <T>{

    T resolve(Class clazz, Field field);
}
