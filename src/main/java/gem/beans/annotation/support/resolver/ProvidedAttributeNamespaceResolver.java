package gem.beans.annotation.support.resolver;

import gem.beans.annotation.Attribute;
import gem.beans.annotation.support.ReflectionDirective;

import java.lang.reflect.Field;

public class ProvidedAttributeNamespaceResolver implements NamespaceResolver {

    @Override
    public String resolve(Class clazz, Field field) {
        Attribute attribute = field.getAnnotation(Attribute.class);
        String namespace = attribute.namespace();
        if (namespace.equals(ReflectionDirective.PACKAGE)) {
            NamespaceResolver resolver = new ReversePackageNamespaceResolver();
            return resolver.resolve(clazz, field);
        } else {
            return attribute.namespace();
        }
    }
}
