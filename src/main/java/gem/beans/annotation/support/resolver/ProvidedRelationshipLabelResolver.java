package gem.beans.annotation.support.resolver;

import gem.beans.annotation.Relationship;

import java.lang.reflect.Field;

public class ProvidedRelationshipLabelResolver implements LabelResolver {

    @Override
    public String resolve(Class clazz, Field field) {
        Relationship relationship = field.getAnnotation(Relationship.class);
        return relationship.value();
    }
}
