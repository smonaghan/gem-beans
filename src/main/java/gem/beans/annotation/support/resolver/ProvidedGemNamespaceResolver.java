package gem.beans.annotation.support.resolver;

import gem.beans.annotation.Gem;
import gem.beans.annotation.support.ReflectionDirective;

import java.lang.reflect.Field;

public class ProvidedGemNamespaceResolver implements NamespaceResolver {
    @Override
    public String resolve(Class clazz, Field field) {
        Gem gem = (Gem) clazz.getAnnotation(Gem.class);
        String namespace = gem.namespace();
        if (namespace.equals(ReflectionDirective.PACKAGE)) {
            NamespaceResolver resolver = new ReversePackageNamespaceResolver();
            return resolver.resolve(clazz, field);
        } else {
            return gem.namespace();
        }
    }
}
