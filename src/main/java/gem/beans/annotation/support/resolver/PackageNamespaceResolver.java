package gem.beans.annotation.support.resolver;

import java.lang.reflect.Field;

public class PackageNamespaceResolver implements NamespaceResolver {

    @Override
    public String resolve(Class clazz, Field field) {
        return clazz.getPackage().getName();
    }
}
