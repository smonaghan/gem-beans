package gem.beans.annotation.support.resolver;

import java.lang.reflect.Field;

public class ClassNameGemTypeResolver implements LabelResolver {

    @Override
    public String resolve(Class clazz, Field field) {
        return clazz.getSimpleName();
    }
}
