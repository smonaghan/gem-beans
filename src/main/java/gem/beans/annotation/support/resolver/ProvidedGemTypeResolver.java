package gem.beans.annotation.support.resolver;

import gem.beans.annotation.Gem;
import gem.beans.annotation.support.ReflectionDirective;

import java.lang.reflect.Field;

public class ProvidedGemTypeResolver implements LabelResolver {

    @Override
    public String resolve(Class clazz, Field field) {
        Gem gem = (Gem) clazz.getAnnotation(Gem.class);
        String type = gem.value();
        if(type.equals(ReflectionDirective.TARGET_NAME)){
            LabelResolver resolver = new ClassNameGemTypeResolver();
            return resolver.resolve(clazz, field);
        }else{
            return type;
        }
    }
}
