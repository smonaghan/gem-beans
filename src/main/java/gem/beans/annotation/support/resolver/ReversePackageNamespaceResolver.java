package gem.beans.annotation.support.resolver;

import java.lang.reflect.Field;

public class ReversePackageNamespaceResolver extends PackageNamespaceResolver {

    @Override
    public String resolve(Class clazz, Field field) {
        String packageName = super.resolve(clazz, field);
        String[] parts = packageName.split("\\.");
        int len = parts.length;
        if (len == 0) return "";

        StringBuffer sb = new StringBuffer();
        for (int i = len; i > 0; i--) {
            System.out.println();
            sb.append(parts[i - 1]);
            sb.append((i > 1) ? "." : "");
        }

        return sb.toString();
    }
}
