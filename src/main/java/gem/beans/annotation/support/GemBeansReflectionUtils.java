package gem.beans.annotation.support;

import gem.beans.annotation.Attribute;
import gem.beans.annotation.Gem;
import gem.beans.annotation.Gid;
import gem.beans.annotation.Relationship;
import gem.beans.annotation.support.resolver.LabelResolver;
import gem.beans.annotation.support.resolver.NamespaceResolver;
import gem.beans.orm.triplestore.Triple;
import gem.beans.orm.triplestore.util.TripleUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;

public class GemBeansReflectionUtils {

    /**
     * Obtains a {@link Gem} {@link Annotation} frm the specified {@link Class}
     *
     * @param clazz The {@link Class} to locate the {@link Gem} on
     * @return
     */
    public static Annotation getGemAnnotation(Class clazz) {
        Annotation[] annotations = clazz.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation.annotationType().getName().equals(Gem.class.getName())) {
                return annotation;
            }
        }

        return null;
    }

    public static Field getGidField(Class clazz) {
        return getFieldHavingAnnotation(clazz, Gid.class);
    }

    public static Collection<Field> getAttributes(Class clazz) {
        return getFieldsHavingAnnotation(clazz, Attribute.class);
    }

    public static Collection<Field> getRelationships(Class clazz) {
        return getFieldsHavingAnnotation(clazz, Relationship.class);
    }

    public static Field getFieldHavingAnnotation(Class clazz, Class annotationClazz) {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            Annotation[] annotations = field.getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation.annotationType().getCanonicalName().equals(annotationClazz.getCanonicalName())) {
                    return field;
                }
            }
        }
        return null;
    }

    public static Collection<Field> getFieldsHavingAnnotation(Class clazz, Class annotationClazz) {
        Field[] fields = clazz.getDeclaredFields();
        Collection<Field> fieldsHaving = new ArrayList<Field>();
        for (Field field : fields) {
            Annotation[] annotations = field.getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation.annotationType().getCanonicalName().equals(annotationClazz.getCanonicalName())) {
                    fieldsHaving.add(field);
                }
            }
        }
        return fieldsHaving;
    }

    // TODO: rethink, the performance implications here are not good...looping through every field
    // looking for the corresponding match for the provided triple sucks. Think about how this
    // can be improved...cache all the Attribute fields for a class could be a quick and dirty
    public static Field getFieldForTriple(Class clazz, Triple triple) throws Exception {
        Collection<Field> fields = getAttributes(clazz);
        for (Field field : fields) {
            Attribute attribute = field.getAnnotation(Attribute.class);

            NamespaceResolver predNsResolver = attribute.namespaceResolver().newInstance();
            String predNs = predNsResolver.resolve(clazz, field);

            LabelResolver predLabelResolver = attribute.labelResolver().newInstance();
            String predLabel = predLabelResolver.resolve(clazz, field);

            if (triple.getPredicate().equals(predNs + "/" + predLabel)) {
                return field;
            }
        }
        return null;
    }

}
