package gem.beans.annotation;

import gem.beans.annotation.support.ReflectionDirective;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to indicate the that host field acts as a unique identifier for the associated
 * {@link Gem}
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Gid {}
