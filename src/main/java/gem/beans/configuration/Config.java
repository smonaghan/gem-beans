package gem.beans.configuration;

import com.thoughtworks.xstream.XStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

public class Config {

    protected final static Logger logger = LoggerFactory.getLogger(Config.class);

    private static Config instance;

    private Defaults defaults;

    public static Config getInstance() {
        if (instance == null) {
            instance = new Config();
        }

        return instance;
    }

    private Config() {
        XStream xstream = new XStream();
        xstream.alias("defaults", Defaults.class);
        URL defaultsFile = this.getClass().getClassLoader().getResource("gem-beans-defaults.xml");
        this.defaults = (Defaults) xstream.fromXML(defaultsFile);
    }

    public Defaults getDefaults() {
        return defaults;
    }
}

