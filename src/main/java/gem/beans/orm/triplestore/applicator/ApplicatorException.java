package gem.beans.orm.triplestore.applicator;

public class ApplicatorException extends RuntimeException {
    public ApplicatorException() {
    }

    public ApplicatorException(String s) {
        super(s);
    }

    public ApplicatorException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ApplicatorException(Throwable throwable) {
        super(throwable);
    }
}
