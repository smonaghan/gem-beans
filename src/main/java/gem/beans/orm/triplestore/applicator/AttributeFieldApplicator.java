package gem.beans.orm.triplestore.applicator;

import gem.beans.annotation.Attribute;
import gem.beans.annotation.Gem;
import gem.beans.annotation.support.GemBeansReflectionUtils;
import gem.beans.annotation.support.resolver.LabelResolver;
import gem.beans.annotation.support.resolver.NamespaceResolver;
import gem.beans.configuration.Config;
import gem.beans.configuration.Defaults;
import gem.beans.orm.triplestore.Triple;
import gem.beans.orm.triplestore.util.TripleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.net.URI;

public class AttributeFieldApplicator implements Applicator<Object, Field, Triple> {

    protected final static Logger logger = LoggerFactory.getLogger(AttributeFieldApplicator.class);

    @Override
    public Triple apply(Object source, Field field, Triple triple) {
        Class clazz = field.getDeclaringClass();
        Attribute attribute = field.getAnnotation(Attribute.class);
        try {

            Gem gem = (Gem) clazz.getAnnotation(Gem.class);

            // assemble Subject
            NamespaceResolver gemjNsResolver = gem.namespaceResolver().newInstance();
            String subjNs = gemjNsResolver.resolve(clazz, null);

            LabelResolver gemTypeResolver = gem.typeResolver().newInstance();
            String gemType = gemTypeResolver.resolve(clazz, null);

            Field gidField = GemBeansReflectionUtils.getGidField(clazz);
            gidField.setAccessible(true);
            String gid = (String) gidField.get(source);

            // subject
            URI subjectUri = TripleUtils.assembleURI(subjNs + "/" + gemType + "#" + gid);
            triple.setSubject(subjectUri.toString());

            // predicate
            NamespaceResolver predNsResolver = attribute.namespaceResolver().newInstance();
            String predNs = predNsResolver.resolve(clazz, field);

            LabelResolver predLabelResolver = attribute.labelResolver().newInstance();
            String predLabel = predLabelResolver.resolve(clazz, field);

            URI predUri = TripleUtils.assembleURI(predNs + "/" + predLabel);
            triple.setPredicate(predUri.toString());

            // object
            field.setAccessible(true);
            triple.setObject(field.get(source));

        } catch (Exception e) {
            throw new ApplicatorException("Unable to apply Field to Triple: " + e.getMessage());
        }

        return triple;
    }
}
