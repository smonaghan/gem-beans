package gem.beans.orm.triplestore.applicator;

public interface Applicator<S, F, T> {
    T apply(S source, F from, T to);
}
