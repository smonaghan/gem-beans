package gem.beans.orm.triplestore;

import gem.beans.annotation.Attribute;
import gem.beans.annotation.Relationship;
import gem.beans.orm.Mapper;
import gem.beans.orm.triplestore.applicator.AttributeFieldApplicator;
import gem.beans.orm.triplestore.assembler.GemAssembler;
import gem.beans.orm.triplestore.support.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Implementation of {@link Mapper} for producing Gems from Triples and Triples from a Gem
 */
public class TripleMapper implements Mapper<Object, Collection<Triple>> {

    protected final static Logger logger = LoggerFactory.getLogger(TripleMapper.class);

    @Override
    public Collection<Object> toGem(Collection<Triple> source) {

        Collection<Object> gems = new ArrayList<Object>();

        Collection<Graph> graphs = new GraphCollector().collect(source);

        GemAssembler assembler = new GemAssembler();

        for (Graph graph : graphs) {
            if (graph.getRdfType() != null) {
                gems.add(assembler.assemble(graph));
            } else {
                logger.info("unable to map graph <" + graph.getRoot() + ">. no " + RDF.RDF_TYPE + " found");
            }
        }

        return gems;
    }

    @Override
    public Collection<Triple> fromGem(Object gem) {
        AttributeFieldApplicator attributeApplicator = new AttributeFieldApplicator();
        // RelationshipFieldApplicator relationshipApplicator = new RelationshipFieldApplicator();
        List<Triple> triples = new ArrayList<Triple>();
        Field[] fields = gem.getClass().getDeclaredFields();

        for (Field field : fields) {
            if (field.getAnnotation(Attribute.class) != null) {
                Triple triple = new Triple();
                triples.add(triple);
                attributeApplicator.apply(gem, field, triple);
            } else if (field.getAnnotation(Relationship.class) != null) {
                Triple triple = new Triple();
                triples.add(triple);

                // TODO: introduce a relationshipApplicator and apply
                //relationshipApplicator.apply(field, triple);
            }
        }

        // TODO: make adoption/declaration of rdf:type 1st class logic - rdf type resolver?
        // for now we're just build a new triple from the first in the Collection to adopt the subject
        // they should all be the same subject as they all originated from the same Gem
        if(!triples.isEmpty()){
            Triple rdfType = new Triple(triples.get(0));
            rdfType.setPredicate(RDF.RDF_TYPE);
            rdfType.setObject(gem.getClass().getName());
            triples.add(rdfType);
        }

        return triples;
    }
}
