package gem.beans.orm.triplestore.support;

public class RDF {
    public static final String RDF_NS = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    public static final String RDF_LN = "rdf";
    public static final String RDF_TYPE = RDF_NS + "type";
    public static final String RDF_TYPE_LN = RDF_LN + ":type";
}
