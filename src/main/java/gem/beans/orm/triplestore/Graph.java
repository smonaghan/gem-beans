package gem.beans.orm.triplestore;

import gem.beans.orm.triplestore.util.TripleUtils;

import java.util.ArrayList;
import java.util.Collection;

public class Graph {

    Collection<Triple> triples;

    private String root;

    private String rdfType;

    public Graph(Triple triple) {
        triples = new ArrayList<Triple>();
        root = triple.getSubject();
        addTriple(triple);
    }

    public Collection<Triple> getTriples() {
        return triples;
    }

    public String getRoot() {
        return root;
    }

    public String getRdfType() {
        return rdfType;
    }

    public void addTriple(Triple triple) {
        this.triples.add(triple);
        if (rdfType == null && TripleUtils.isRdfType(triple)) {
            rdfType = triple.getObject().toString();
        }
    }
}
