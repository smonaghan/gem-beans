package gem.beans.orm.triplestore;

import java.util.UUID;

public class Triple {

    public static enum PART {SUBJECT, PREDICATE, OBJECT}

    private String id;

    private String subject;

    private String predicate;

    private Object object;

    public Triple() {
        this.id = UUID.randomUUID().toString();
    }

    public Triple(String subject, String predicate, Object object) {
        this();
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
    }

    public Triple(Triple triple) {
        this.setSubject(triple.getSubject());
        this.setPredicate(triple.getPredicate());
        this.setObject(triple.getObject());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPredicate() {
        return predicate;
    }

    public void setPredicate(String predicate) {
        this.predicate = predicate;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "Triple{" +
                "subject='" + subject + '\'' +
                ", predicate='" + predicate + '\'' +
                ", object=" + object +
                '}';
    }
}
