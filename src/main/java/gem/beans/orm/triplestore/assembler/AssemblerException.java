package gem.beans.orm.triplestore.assembler;

public class AssemblerException extends RuntimeException {

    public AssemblerException() {
    }

    public AssemblerException(String s) {
        super(s);
    }

    public AssemblerException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public AssemblerException(Throwable throwable) {
        super(throwable);
    }
}
