package gem.beans.orm.triplestore.assembler;

import gem.beans.annotation.Attribute;
import gem.beans.annotation.support.GemBeansReflectionUtils;
import gem.beans.orm.triplestore.Graph;
import gem.beans.orm.triplestore.Triple;
import gem.beans.orm.triplestore.support.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

public class GemAssembler {

    protected final static Logger logger = LoggerFactory.getLogger(GemAssembler.class);

    public Object assemble(Graph graph) throws AssemblerException {
        if (graph.getRdfType() != null) {
            try {
                Class clazz = Class.forName(graph.getRdfType());
                Object gem = clazz.newInstance();
                for (Triple triple : graph.getTriples()) {
                    if(triple.getPredicate().equals(RDF.RDF_TYPE)) continue;
                    Field field = null;
                    try {
                        field = GemBeansReflectionUtils.getFieldForTriple(clazz, triple);
                        field.setAccessible(true);
                        field.set(gem, triple.getObject());
                    } catch (Exception e) {
                        e.printStackTrace();
                        logger.error(e.getMessage());
                    }

                }

                return gem;

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                throw new AssemblerException(e);
            } catch (InstantiationException e) {
                e.printStackTrace();
                throw new AssemblerException(e);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                throw new AssemblerException(e);
            }
        } else {
            String msg = "Could not assemble Gem. No rdf:type available when reading Graph";
            logger.trace(msg);
            throw new AssemblerException(msg);
        }
    }
}
