package gem.beans.orm.triplestore.util;

import gem.beans.configuration.Config;
import gem.beans.configuration.Defaults;
import gem.beans.orm.triplestore.Triple;
import gem.beans.orm.triplestore.support.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static gem.beans.orm.triplestore.Triple.PART.*;

/**
 * Dirty util to capture intent during initial development. Consider a TripleReader or other
 */
public class TripleUtils {

    protected final static Logger logger = LoggerFactory.getLogger(TripleUtils.class);

    private static Defaults defaults = Config.getInstance().getDefaults();

    public static URI assembleURI(String uriString) {
        try {
            URI uri = new URI(uriString);
            if (uri.getScheme() == null) {
                uri = new URI(defaults.getGemScheme() + "://" + uriString);
            }
            return uri;
        } catch (URISyntaxException e) {
            logger.trace("could not produce value URI from uriString <" + uriString + ">");
            throw new RuntimeException(e);
        }
    }

    public static Set<String> getDistinctSubjects(Collection<Triple> triples) {
        Set<String> subjects = new HashSet<String>();
        for (Triple triple : triples) {
            subjects.add(triple.getSubject());
        }

        return subjects;
    }

    public static Set<String> getDistinctPredicates(Collection<Triple> triples) {
        Set<String> predicates = new HashSet<String>();
        for (Triple triple : triples) {
            predicates.add(triple.getPredicate());
        }

        return predicates;
    }

    public static Set<Object> getDistinctObjects(Collection<Triple> triples) {
        Set<Object> objects = new HashSet<Object>();
        for (Triple triple : triples) {
            objects.add(triple.getObject());
        }

        return objects;
    }


    public static boolean isRdfType(Triple triple){
        return (triple.getPredicate().equals(RDF.RDF_TYPE) ||
        triple.getPredicate().equals(RDF.RDF_TYPE_LN));
    }


}
