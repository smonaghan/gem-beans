package gem.beans.orm.triplestore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class GraphCollector {

    protected final static Logger logger = LoggerFactory.getLogger(GraphCollector.class);

    public Collection<Graph> collect(Collection<Triple> triples) {

        Map<String, Graph> graphByRoot = new HashMap<String, Graph>();

        for (Triple triple : triples) {

            String subject = triple.getSubject();

            Graph graph = graphByRoot.get(subject);

            if (graph == null) {
                graph = new Graph(triple);
                graphByRoot.put(subject, graph);
            }else{
                graph.addTriple(triple);
            }

        }

        return graphByRoot.values();
    }
}
