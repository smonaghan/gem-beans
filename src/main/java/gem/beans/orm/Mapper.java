package gem.beans.orm;

/**
 * Defines core interface for mapping type T to type G, and from type G to type T
 * @param <G>
 * @param <T>
 */
public interface Mapper<G, T> {

    G toGem(T source);

    T fromGem(G gem);
}
